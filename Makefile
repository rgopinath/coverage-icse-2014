all: build/paper.pdf

build/paper.pdf: src/paper.Rnw
	mkdir -p build/data
	cp -r data/* build/data/
	cd build/data; cat *.tar.gz| gzip -dc | tar -xvpf - ; R CMD INSTALL Coverage
	cp etc/* build
	cp src/*.Rnw build
	cp src/*.bib build
	cd build; Rscript -e "require(knitr); knit('paper.Rnw', encoding='UTF-8');"
	cd build; ../bin/latexmk -pdf acm_sigproc.tex

update:
	git pull --rebase origin icse1

push:
	git push origin icse1

clean:
	rm -rf build

html: all
	#cd build/figure; for i in *.pdf; do convert $$i $${i%.pdf}.eps; done
	cd build; ../stripfigs.rb paper.tex > x.tex ; mv x.tex paper.tex; htlatex acm_sigproc.tex
