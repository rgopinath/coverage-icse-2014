#!/usr/bin/ruby

lines = File.readlines(ARGV[0])

i = 0
skip = false
while true do
  l = lines[i]
  break if l.nil?
  case l
  when /begin.*figure/
    skip = true
  when /end.*figure/
    skip = false
  else
    puts l unless skip
  end
  i += 1
end
